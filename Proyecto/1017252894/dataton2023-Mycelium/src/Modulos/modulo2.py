#Importar modulos necesarios
import numpy as np
import time
from joblib import Parallel, delayed
from functools import partial

#Importa los modulos creados
from Modulos.modulo1 import *


class Horario(object):
    """
    Esta clase se encarga de generar un horario optimizado para cada sucursal
    
    Atributos:
    ----------
    tiempo_opti: int
        Tiempo en segundos que se le da a cada proceso de optimización
        por cada nucleo del cpu
    total_optimizaciones: int
        Numero de optimizaciones que se le hacen al horario
        
    Metodos:
    --------
    CalcularHorario(horario,ultima_semana,ultima_sabado)
        Calcula un horario de trabajo para cada sucursal
        
    Optimizacion(horario,ultima_semana,ultima_sabado)
        Optimiza el horario de trabajo para una sucursal
        
    opti(horario,ultima_semana,ultima_sabado)
        Optimiza el horario de una sucursal paralelizando la función Optimizacion
        
    _CalcularPuntaje(horario)
        Calcula el puntaje del horario
        
    _Correcto(horario)
        Determina si el horario es correcto
        
    """

    def __init__(self) -> None:
        """
        Inicializa los atributos de la clase
        
        Atributos:
        ----------
        tiempo_opti: int
            Tiempo en segundos que se le da a cada proceso de optimización
            por cada nucleo del cpu
            
        total_optimizaciones: int
            Numero de optimizaciones que se le hacen al horario
            
        num_procesos: int
            Numero de procesos que se le asignan a cada optimización
        
        """
        self.total_optimizaciones = 5
        self.tiempo_opti = 3
        self.num_procesos = 4
        pass

    def _CalcularPuntaje(self,horario):
        """
        Esta función calcula el puntaje del horario
        
        Parametros:
        -----------
        horario: Horario de trabajo
        
        Retorna:
        --------
        puntaje: Puntaje del horario
        """
        return CalcularPuntaje(horario)

    def _Correcto(self,horario):
        """
        Esta función determina si el horario es correcto
        
        Parametros:
        -----------
        horario: Horario de trabajo
        
        Retorna:
        --------
        correcto: True si el horario es correcto, False de lo contrario
        """
        return Correcto(horario)

    def Optimizacion(self,horario,ultima_semana,ultima_sabado):
        """
        Esta función optimiza el horario de trabajo para una sucursal
        
        Parametros:
        horario: Horario que se desea optimizar
        ultima_semana: Ultima hora a la que hay demanda en semana
        ultima_sabado: Ultima hora a la que hay demanda el sabado
        
        Retorna:
        --------
        horario: Horario para una sucursal optimizado
        
        """
        empleados = len(horario[0])-3
        puntaje = CalcularPuntaje(horario)
        inicio = time.time()
        
        # Se toma al primer empleado
        emp = 0
        while time.time() - inicio < self.tiempo_opti:
            
            # Se crea la copia1
            # A esta copia se le genera una nueva hora de entrada y almuerzo para el empleado 'emp'
            horario_temp1 = horario.copy()
            for i in range(25):
                # Se calcula un nuevo horario con las nuevas horas de entrada y almuerzo
                while True:
                    # Generar unas nuevas hora para entrada y almuerzo
                    horario_temp1[3][emp] = -1
                    horario_temp1[4][emp] = 0
                    
                    # Calcular un nuevo horario con estas nuevas horas
                    for dia in range(6):
                        CalcularJornada(horario_temp1[:,emp],dia,ultima_semana,ultima_sabado)
                        CalcularDemanda(horario_temp1)
                    
                    puntaje1 = CalcularPuntaje(horario_temp1)
                    if (Correcto(horario_temp1)):break

                # Se crea la copia2 de la copia1
                horario_temp2 = horario_temp1.copy()
                for dia in range(6):
                    # Para esta nueva copia2 se calcula la jornada dia por dia
                    for i in range(10):
                        # Para cada dia se calcula una nueva jornada
                        CalcularJornada(horario_temp2[:,emp],dia,ultima_semana,ultima_sabado)
                        CalcularDemanda(horario_temp2)
                        puntaje2 = CalcularPuntaje(horario_temp2)

                        # Si copia2 con la nueva jornada tiene menor puntaje que la copia1,
                        # se actualiza la copia1
                        if puntaje2 <= puntaje1 and Correcto(horario_temp2):
                            horario_temp1 = horario_temp2.copy()
                            puntaje1 = puntaje2

                # Si la copia1 tiene menor puntaje que el horario original,
                # se actualiza el horario original
                if puntaje1 <= puntaje and Correcto(horario_temp1):
                    horario = horario_temp1.copy()
                    puntaje = puntaje1
            
            # Se pasa al siguiente empleado
            emp += 1

            # Cuando se llegue al ultimo empleado se vuelve al primero
            if emp == empleados:emp = 0

        return horario
    
    def opti(self,horario,ultima_semana,ultima_sabado):
        """
        Esta función optimiza el horario a partir de una paralelización de la función Optimizacion
        
        Parametros:
        horario: Horario que se desea optimizar
        ultima_semana: Ultima hora a la que hay demanda en semana
        ultima_sabado: Ultima hora a la que hay demanda el sabado
        
        Retorna:
        --------
        horario: Horario optimizado para una sucursal
        
        """
        empleados = len(horario[0])-3


        for _ in range(self.total_optimizaciones):
            posibles_horarios = [horario.copy() for i in range(self.num_procesos)]
            fun_par = partial(self.Optimizacion,ultima_semana=ultima_semana,ultima_sabado=ultima_sabado)

            def aplicar_fun_par(x):
                return fun_par(x)
            
            posibles_horarios = Parallel(n_jobs=self.num_procesos)(delayed(aplicar_fun_par)(i) for i in posibles_horarios)
            # Se optimiza cada copia con cada nucleo del cpu
            
            # Dado que el proceso de optimizacion es por montecarlo, cada copia puede tener un puntaje distindo
            posibles_puntajes = [CalcularPuntaje(posibles_horarios[i]) for i in range(self.num_procesos)]
            indices = [i for i in range(len(posibles_puntajes)) if posibles_puntajes[i]==min(posibles_puntajes)]
            indice_min = int(random.choice(indices))
            
            # Se toma el horario con el menor puntaje y se reemplazan los demás y se repite el proceso
            horario = posibles_horarios[indice_min]

        return horario


    def CalcularHorario(self,horario,ultima_semana,ultima_sabado):
        """
        Esta función calcula un horario de trabajo para cada sucursal
        
        Parametros:
        horario: Horario que se desea optimizar
        ultima_semana: Ultima hora a la que hay demanda en semana
        ultima_sabado: Ultima hora a la que hay demanda el sabado
        
        Retorna:
        --------
        horario: Horario optimizado para una sucursal
        
        """
        empleados = len(horario[0]) - 3

        # Determinar cuales son los empleados TC y MT
        ind4 = [i for i in range(len(horario[0])) if horario[1][i] == 4]
        ind8 = [i for i in range(len(horario[0])) if horario[1][i] == 8]
        
        horarios = [horario.copy() for i in range(4)]
        # Se definen las distintas combinaciones entre quien entra a primera hora 
        # y quien sale a ultima hora
        for J,hor in enumerate(horarios):
            if J==0:
                # Ambos son TC
                hor[2][ind8[0]] = 1
                hor[2][ind8[1]] = 2

            elif J==1:
                # Un TC entra a primera hora
                # Un MT sale a ultima hora
                hor[2][ind8[0]] = 1
                hor[2][ind4[0]] = 2

            elif J==2:
                # Un MT entre a primera hora
                # Un TC sale a ultima hora
                hor[2][ind4[0]] = 1
                hor[2][ind8[1]] = 2

            elif J==3:
                # Ambos son TC
                hor[2][ind4[0]] = 1
                hor[2][ind4[1]] = 2

        # Calcular un horario inicial para cada posibilidad 
        for hor in horarios:
            while True:
                hor[3] = -1
                hor[4] = 0
                for emp in range(empleados):
                    for dia in range(6):
                        CalcularJornada(hor[:,emp],dia,ultima_semana,ultima_sabado)
                        CalcularDemanda(hor)
                puntaje = CalcularPuntaje(hor)
                if Correcto(hor): break

        # Puntaje de cada posible horario
        puntajes = [CalcularPuntaje(horarios[i]) for i in range(len(horarios))]

        # Optimizar cada horario
        for i in range(len(horarios)):
            horarios[i] = self.opti(horarios[i],ultima_semana,ultima_sabado)
            puntajes[i] = CalcularPuntaje(horarios[i])

            #print("Puntajes:",puntajes)

        # De las 4 posibilidades, se retorna la que menor puntaje tenga
        puntaje = min(puntajes)
        indice = puntajes.index(puntaje)
        horario = horarios[indice]

        return horario