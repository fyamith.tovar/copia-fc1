import numpy as np
import matplotlib.pyplot as plt

class trayCargada:
    '''
    Clase para encontrar la trayectoria que sigue una particula cargada dadas:

    B: Magnitud del campo eléctrico. Número real distinto de cero. Si es positivo la dirección del campo sería en +zy viceversa. 
        Debe estar en Tesla. Ingrese unint o float.
    theta: Ángulo entre el vector velocidad inicial de la partícula y el eje z. Número real. Si es cero
        La trayectoria será una línea recta en el eje z. Debe estar en grados. Ingrese int o float. 
    Ek: Energía cinética con la que parte la partícula. Debe estar en eV. Número real positivo (si ingresa 0 se 
        asume que no hay movimiento). Ingrese un int o float.
    
    Las siguientes variables ya tienen asigando un valor: 

    m: Masa de la particula, se asume que es el electrón. Está en kg. Si se cambia debe ser un real positivo 
        distinto de cero. Ingrese un int o float.
    q: Carga de la particula, se asume que es el electrón. Está en Coulomb. debe ser un real positivo.
        Ingrese un int o float.
    tmax: Tiempo máximo en el que se evalúa la trayectoria. Está fijado en 10 segundos. Si se cambia debe ser 
        un real positivo. Ingrese un int o float.
    dt: Tamaño del paso de tiempo con los que se evalúan las trayectorias. Está fijado en 0.1 segundos.
         Si se cambia debe ser un entero positivo. Para un tamaño menor a 0.1 no se puede ver claramente la 
         forma de la trayectoria.

    '''

    def __init__(self,B,theta,Ek,m = 9.109e-31,q = 1.602e-19,tmax=100,dt=0.1):
        self.B = B
        self.ek = Ek
        self.theta = theta
        self.m = m
        self.q = q
        self.tmax = tmax
        self.dt = dt
        self.x = [] # trayectoria en x
        self.y = [] # trayectoria en y
        self.z = [] # trayectoria en z
        self.t = [] # paso del tiempo

    def w(self): # frecuencia angular de la trayectoria xy, llamada frecuencia cicloide
        try:
            return self.q*self.B / self.m 
        except:
            print(' \n \nNo se pudo calcular la frecuencia.\
                  \nRevise que los parámetros de entrada cumplan las condiciones establecidas.')
            raise SystemExit(0)
    
    def v0(self): # magnitud velocidad inicial de la particula
        try:
            return np.sqrt(2*self.ek*1.602e-19/self.m) #  \nNotese que la energia se pasa de eV a J
        except:
            print(' \nNo se pudo calcular la velocidad inicial.\
                  \nRevise que los parámetros de entrada cumplan las condiciones establecidas.')
            raise SystemExit(0)
        
    def v0x(self): # magnitud velocidad inicial de la particula en x
        try:
            return self.v0()*np.sin(np.deg2rad(self.theta)) # Notese que el ángulo se pasa de ° a radianes
        except:
            print(' \nNo se pudo calcular la velocidad inicial en x.\
                  \nRevise que los parámetros de entrada cumplan las condiciones establecidas.')
            raise SystemExit(0)
    
    def v0z(self): # magnitud velocidad inicial de la particula en z
        try:
            return self.v0()*np.cos(np.deg2rad(self.theta)) #  Notese que el ángulo se pasa de ° a radianes
        except:
            print(' \nNo se pudo calcular la velocidad inicial en z.\
                  \nRevise que los parámetros de entrada cumplan las condiciones establecidas.')
            raise SystemExit(0)
    # Se asume que magnitud velocidad inicial de la particula en y es 0 gracias a una escogencia conveniente
        # del sistema de referencia

    def tray(self): # trayectoria en x,y,z
        try:
            self.t = np.arange(0,self.tmax+self.dt,self.dt)
            if self.B == 0 or self.q == 0:
                self.z = self.v0z()*self.t
                self.x = self.v0x()*self.t
                self.y = np.zeros(len(self.t))
                return self.x,self.y,self.z
            else:
                self.z = self.v0z()*self.t
                self.x = (self.v0x()/self.w()) * np.sin(self.w()*self.t)
                self.y = (self.v0x()/self.w()) * (np.cos(self.w()*self.t)-1)
                return self.x,self.y,self.z
                
        except:
            print(' \nNo se pudo calcular las trayectorias.\
                  \nRevise que los parámetros de entrada cumplan las condiciones establecidas.')
            raise SystemExit(0)

    def graphxyz(self): # gráfica de la trayectoria en el espacio
        try:
            x,y,z = self.tray()
            fig = plt.figure(figsize=(8,8))
            ax = fig.add_subplot(111,projection='3d')
            ax.plot(x, y, z,c='tab:pink')
            ax.set_title('Trayectoria de la partícula')
            ax.set_xlabel('X [m]')
            ax.set_ylabel('Y [m]')
            ax.set_zlabel('Z [m]')
            plt.savefig('Trayectoria_xyz.png')
            plt.show()
        except:
            print(' \nNo se pudo graficar la trayectoria.\
                  \nRevise que los parámetros de entrada cumplan las condiciones establecidas.')
            raise SystemExit(0)