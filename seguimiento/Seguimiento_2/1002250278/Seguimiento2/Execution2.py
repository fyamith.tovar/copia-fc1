from Problema2 import Boltzmann
import numpy as np

if __name__ == '__main__':
    
    e = [0, 1, 2, 3, 4]
    T = 5
    n = 10000

    sol = Boltzmann(e, T, n)
    A=sol.Probabilidades()
    sol.Graficos()
   
