Instructivo KernelSDGC

Asegurarse de correr colab en version 'Python 3.10.12'

Instrucciones de uso:
La libreria tiene 1 clase:

1) Kernels:

   La clase recibe 2 argumentos
   1) url             : recibe el url con el .csv
   2) param           : Recibe el parametro a evaluar

   La clase tiene 6 métodos
   1) Dataframe	      : Lee el .csv y da orden a la informacion
   2) Tricube         : Calcula kernel Tricube
   3) Epane           : Calcula kernel Epanechnikov
   4) Gaussiano       : Calcula kernel Gaussiano      
   5) GraphTri        : Grafica ajuste Tricube       
   6) GraphEpane      : Grafica ajuste Epanechnikov
   7) GraphGauss      : Grafica ajuste Gaussiano
   8) GraphAll        : Grafica todos los ajustes


CODIGO SUGERIDO:

!pip install KernelSDGC

import KernelSDGC as ker
u = ""                                          # Favor descargar el archivo y cargarlo localmente, no me sirve url, no se pq
V = ker.Kernels(url=u, param =10)
# Para graficar todo
V.GraphAll() 
# Para graficar Tricube
V.GraphTri()
# Para graficar Epanechnikov
V.GraphEpane()
# Para graficar Gaussiano
V.GraphGauss()